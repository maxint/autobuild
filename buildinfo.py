#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Platform utilities for auto build system.

common infos:
    platform - 'windows', 'linux', 'osx', 'android', 'ios', 'tizen'
    arch - 'arm', 'arm64', 'x86', 'x64'
    sdk - 'vc6', 'vs2012', 'ndk-r8e', 'tizen-sdk-2.2'

    platform_abbr - 'win32', 'win64'
    compiler_ver - compiler version
    compiler_abbr - <compiler><compiler_ver>

'''


def get_platform_abbr(platform, arch):
    if platform == 'windows':
        if arch == 'x86':
            return 'win32'
        elif arch == 'x64':
            return 'win64'
    return platform + '_' + arch


def remove_empty(binfos):
    '''remove empty items.'''
    # remove None item
    binfos = dict((k, v) for k, v in binfos.iteritems() if not v is None)


def fill_infos(binfos, cflags=None):
    '''fill other auxilliary informations.'''
    # save results
    binfos['platform_abbr'] = get_platform_abbr(binfos['platform'],
                                                binfos['arch'])

    arch = binfos['arch']
    abi = arch
    if cflags and arch == 'arm' or arch == 'arm64':
        import re
        m = re.search(r'\b(armv\d-?[a-z]?)\b', cflags)
        if m:
            abi = m.group(1)
    binfos['abi'] = abi
