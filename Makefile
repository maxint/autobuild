#
# Makefile
# maxint, 2014-08-27 22:00
#

all: clean android_arm android_arm64 linux32 linux64 android_arm64_stl

clean:
	@rm example/_obj -rf

android_arm:
	./main.py -p android -a arm example/project.yaml -V

android_arm64:
	./main.py -p android -a arm64 example/project.yaml -V -e "depends=mpbase 0.1.0.4 shared"

android_arm64_stl:
	./main.py -p android -a arm64 example/project_cxx.yaml -V -e "depends=mpbase 0.1.0.4 shared&sdk=ndk64-r10c-gnustl_static" -V1

linux32:
	./main.py -p linux -a x86 example/project.yaml -V -e "depends=mpbase 0.1.0.3"

linux64:
	./main.py -p linux -a x64 example/project.yaml -V -e "depends=mpbase 0.1.0.3"


# vim:ft=make
#
