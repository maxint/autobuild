#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Make SDK package.
'''

import version
import sdkutil
import os
import shutil
import re


def get_full_package_name(fname, platform):
    '''Get full package name of a library file'''
    verstr = version.get_full_version_number(fname)
    datestr = sdkutil.get_format_date('%m%d%Y')
    platform = platform.replace('/', '_')
    full_name = '{}_{}_{}'.format(verstr, platform, datestr)
    return full_name.upper()


def get_lib_locations(libdir):
    paths = sdkutil.glob_libraries(libdir)
    return filter(lambda x: not os.path.basename(x).startswith('_'), paths)


def filter_cflags(cflags):
    # remove path with quote surround
    cflags = re.sub(r'"[^"]*"', '""', cflags)
    # filter cflags
    cflags = cflags.split(' ')

    def cflags_filter(x):
        if x == '' or len(x) > 10 and os.path.exists(x):
            return False
        IGNORES = ['--sysroot',
                   '-I', '/I',
                   '-gcc-toolchain',
                   '-std=c++',
                   '-isystem']
        for i in IGNORES:
            if x.startswith(i):
                return False
        return True
    cflags = filter(cflags_filter, cflags)
    cflags = sdkutil.f7(cflags)

    return ' '.join(cflags)


def makepkg(platform,
            releasenote, libpath, cflags, depends,
            incs, samplecodes, docs,
            bdir, libsubdir=None):
    '''Create ZIP SDK package'''
    # get build number and create dist directory if needed
    vers0 = version.get_lite_version_number(libpath, True)
    vers1 = version.get_lite_version_number(releasenote)
    assert vers0 == vers1, "{} != {}".format(vers0, vers1)

    # create temporary package directory and collect files
    fullpkgname = get_full_package_name(libpath, platform)
    libs = get_lib_locations(os.path.dirname(libpath))
    assert len(libs) > 0
    if libsubdir is None:
        libsubdir = platform
    pairs = [
        (libs, 'lib/{}/'.format(libsubdir)),
        (incs, 'inc/'),
        (docs, 'doc/'),
        (samplecodes, 'samplecode/'),
        (releasenote, 'releasenotes.txt')
    ]
    tmpdir = os.path.join(bdir, '__tmp__')
    if os.path.exists(tmpdir):
        shutil.rmtree(tmpdir)
    sdkdir = tmpdir + os.sep + fullpkgname
    sdkutil.copy_files(pairs, sdkdir, force=True)
    for dep in depends or []:
        pairs = [(dep['incs'], 'inc/'), (dep['libs'], 'lib/')]
        pkgpath = tmpdir + os.sep + dep['pkgpath'].upper()
        sdkutil.copy_files(pairs, pkgpath, force=True)

    cflags = filter_cflags(cflags)

    # update releasenotes
    rlsnote_path = sdkdir + os.sep + 'releasenotes.txt'
    verstr = version.get_version_number(libpath)
    import releasenote as rlsnote
    content = rlsnote.get_releasenote(sdkdir,
                                      releasenote,
                                      platform,
                                      cflags,
                                      verstr)
    open(rlsnote_path, 'wt').write(content)

    # zip directory
    zippath = os.path.join(bdir, fullpkgname + '.ZIP')
    sdkutil.zip(zippath, tmpdir)

    return zippath
