#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Parse project file in YAML.

all:
{
    // common properties
    version:
    authors: []

    // for target
    name:
    type:
    inc_dirs: []
    srcs: [[]]
    cflags:
    cppflags:
    ldflags:
    macros: []
    depends: []
    enabled: []

    // for version number
    version_file:
    releasenote:

    dists: []
    tests: []
}

dist:
{
    docs: []
    samplecodes: []
    incs: []
}

test:
{
    // for target, as in build
    // depend on build by default
}


"""

try:
    import yaml
except:
    import sys
    import binary
    sys.path.insert(0, binary.getpath('yaml.zip'))
    import yaml

import logging

log = logging.getLogger('build')


def load(binfos, fname):
    log.debug('Parsing %s ...', fname)
    with open(fname, 'rt') as f:
        m = yaml.safe_load(f)
        version = m.get('version', 1)
        mname = 'project' + str(version)

        log.debug('Auto build version: %d', version)
        log.debug('Import ' + mname)

        try:
            proj = __import__(mname)
        except:
            raise Exception('Version {} is not supported'.format(version))

        bdict = proj.load(binfos, m)
        bdict['version'] = version
        binfos['project'] = bdict

        log.debug('Parsing %s finished', fname)

        return bdict

if __name__ == '__main__':
    pass
