#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Utility functions for SDK packaging.
'''

import os
import glob
import shutil

# Auxiliary functions {{1


def sleep(duration=1):
    import time
    time.sleep(duration)


def get_format_date(fmt):
    '''Get formated date string'''
    from datetime import date
    d = date.today()
    return d.strftime(fmt)


def mkdirs(tdir):
    if tdir and not os.path.exists(tdir):
        os.makedirs(tdir)


def emptydir(tdir):
    for item in os.listdir(tdir):
        path = os.path.join(tdir, item)
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)


def glob_by_exts(dpath, exts):
    paths = []
    for ext in exts:
        paths += glob.glob(os.path.join(dpath, '*.' + ext)) or []
    return paths


def glob_libraries(dpath):
    return glob_by_exts(dpath, ['dll', 'lib', 'so', 'a', 'dylib'])


def lazy_write(fname, s):
    tdir = os.path.dirname(fname)
    mkdirs(tdir)

    if os.path.exists(fname):
        olds = open(fname, 'rt').read()
        if olds != s:
            os.remove(fname)
    if not os.path.exists(fname):
        print 'Writing {}'.format(fname)
        return open(fname, 'wt').write(s)


# http://stackoverflow.com/questions/3041986/python-command-line-yes-no-input
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is one of "yes" or "no".
    """
    import sys
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


# http://stackoverflow.com/questions/480214/how-do-you-remove-duplicates-from-a-list-in-python-whilst-preserving-order
def f7(seq):
    seen = set()
    return [x for x in seq if not (x in seen or seen.add(x))]

# SDK packaging {{1


def clear_file_pairs(pairs):
    '''Convert to clean (source, target) list'''
    zpairs = []
    for (src, dst) in pairs:
        if type(src) is list:
            for f in src:
                zpairs.append((f, os.path.join(dst, os.path.basename(f))))
        elif os.path.isfile(src):
            if not dst or dst.endswith('/'):
                dst = os.path.join(dst, os.path.basename(src))
            zpairs.append((src, dst))
        elif os.path.isdir(src):
            for root, dirs, files in os.walk(src):
                for f in files:
                    src = os.path.join(root, f)
                    zpairs.append((src, os.path.join(dst, f)))
        elif '*' in src:
            for f in glob.glob(src):
                zpairs.append((f, os.path.join(dst, os.path.basename(f))))

    return zpairs


def copy_files(pairs, dstdir, debug=False, force=False):
    '''Copy (source, target) list to destination directory'''
    if os.path.exists(dstdir):
        if force:
            shutil.rmtree(dstdir)
        else:
            if query_yes_no('Delete "{}" firstly?'.format(dstdir)):
                print "Removing '{}'.".format(dstdir)
                shutil.rmtree(dstdir)
            else:
                return False

    print 'Creating "{}"'.format(dstdir)
    os.makedirs(dstdir)
    for (src, dst) in clear_file_pairs(pairs):
        print '  {} -> {}'.format(src, dst)
        dst = dstdir + os.sep + dst
        ddir = os.path.dirname(dst)
        if not os.path.exists(ddir):
            os.makedirs(ddir)
        shutil.copyfile(src, dst)

    return True


def zip(zipfname, target):
    '''zip directory or (source, target) list to a file named zipfname'''
    import zipfile
    zfile = zipfile.ZipFile(zipfname, 'w')
    print 'Ziping "{}"'.format(zfile.filename)
    if type(target) is dict:
        for (src, dst) in clear_file_pairs(target):
            zfile.write(src, dst)
    elif type(target) is str:
        for root, dirs, files in os.walk(target):
            for f in files:
                zpath = os.path.join(root[len(target) + 1:], f)
                zfile.write(os.path.join(root, f), zpath)
    else:
        assert False, 'Unsupported input of ziping target'

    zfile.close()


def unzip(zipfname, targetdir='.'):
    '''unzip to targetdir'''
    import zipfile
    zfile = zipfile.ZipFile(zipfname)
    print targetdir
    print 'Unziping "{}" to "{}"'.format(zfile.filename, targetdir)
    for name in zfile.namelist():
        (dirname, filename) = os.path.split(name)
        if targetdir:
            dirname = os.path.join(targetdir, dirname)
        print '  Decompressing {} on {}'.format(filename, dirname)
        if not os.path.exists(dirname) and dirname != '':
            os.makedirs(dirname)
        zfile.extract(name, targetdir)
    zfile.close()

# Main {{1

if __name__ == '__main__':
    print get_format_date('%m/%d/%Y')
