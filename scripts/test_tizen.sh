#! /bin/bash
#
# test_tizen.sh
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.
#

DSTDIR=/home/sbs/lny
cp -u ../lib/tizen/libarcsoft_vdoinvideo.so $DSTDIR
cp -u ../obj/tizen/test $DSTDIR
cp -u ../data/1280x720.NV21 $DSTDIR
./expect.sh "" ~/sbs-install/bin/sbs -e "cd $DSTDIR && chmod 777 test && export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:. && ./test"
