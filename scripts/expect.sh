#!/usr/bin/expect -f

set timeout 60
spawn -noecho [lindex $argv 1] [lrange $argv 2 end-1]
set passwd [lindex $argv 0]
set command [lindex $argv end]

#send -- $passwd\n
#send -- $command\n

while true {
    expect {
        eof			{break}
        "(yes/no)?" {send "yes\r"}
        "*assword:" {send "$passwd\r"}
        "*\]"       {
            send "$command\r"
            send "exit\r"
        }
    }
}
wait
