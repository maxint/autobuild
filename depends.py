#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Get include directory and library path of dependency.
'''

import re
import mpbase

import logging
log = logging.getLogger('build')

# (name, version) regexes
NAME_VER_RE_LIST = [
    re.compile(r'^(?P<name>\S+)\s+[^\d]*v?(?P<ver>[0-9.]+)'),
    re.compile(r'^(?P<name>\S+)\s+[^\d]*\(\s*(?P<ver>[0-9.]+)\s*\)')
]


def split_name_version(fullname):
    '''fullname formats:
        - mpbase v0.1.0.3 [shared|static]   static by default
        - mpbase 0.1.0.3 [shared|static]    static by default
        - mpbase [shared|static] 0.1.0.3    static by default
        - mpbase (0.1.0.3) [shared|static]
    '''
    for r in NAME_VER_RE_LIST:
        m = r.match(fullname)
        if m:
            return m.group('name'), m.group('ver')
    if not re.match(r'^\w+$', fullname):
        log.error('Invalid dependency name: %s', str(fullname))
        raise Exception('Invalid dependency name: ' + fullname)

    return fullname, None


def get(binfos, depends):
    dm = binfos['config']['depends']
    dlist = list()
    for dep in depends:
        name, ver = split_name_version(dep)
        if re.search(r'\bshared\b', dep):
            shared = True
        elif re.search(r'\bstatic\b', dep):
            shared = False
        else:
            shared = None
        log.debug('Loading depend %s (%s) shared:%s',
                  name, str(ver), str(shared))
        ldd = dict(name=name, version=ver, shared=shared)
        ldd.update(mpbase.incs_libs2(binfos, dm.get(name), ver, shared))
        dlist.append(ldd)

    return dlist


def get_incs_libs(dlist):
    incs = [d.get('inc_dir') for d in dlist]
    libs = [d.get('libs') for d in dlist]
    return incs, libs


def fetch_incs_libs(binfos, depends):
    return get_incs_libs(get(binfos, depends))


if __name__ == '__main__':
    binfos = {
        'platform': 'android',
        'arch': 'arm',
        'compiler_abbr': None
    }
    binfos1 = {
        'platform': 'windows',
        'arch': 'x86',
        'compiler_abbr': 'vs2012',
    }
    import config
    cfg = config.load()
    binfos.update(cfg)
    binfos1.update(cfg)

    print fetch_incs_libs(binfos, ['mpbase v0.1.0.3'])
    print fetch_incs_libs(binfos, ['mpbase (0.1.0.3)'])
    print fetch_incs_libs(binfos, ['mpbase'])
    print fetch_incs_libs(binfos1, ['mpbase'])
