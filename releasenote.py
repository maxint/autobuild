#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Generate whole release notes.
'''

import dirtree
import version as ver
import sdkutil
import re


def sub(title, newval, s):
    return re.sub(r'\b({}:\s*\n)[\s\S]*?\n\s*\n'.format(title),
                  r'\g<1>{}\n\n'.format(newval), s)


def get_releasenote(sdkdir, template, platform, cflags='', version=''):
    dtree = dirtree.list_files(sdkdir).strip()
    s = open(template, 'r').read()
    if not version or version.isspace():
        version = ver.get_version_number(template)

    s = sub('Publish date', sdkutil.get_format_date("%m/%d/%Y"), s)
    s = sub('Version', version, s)
    s = sub('Supported platforms', platform.upper(), s)
    s = sub('Compile Option', cflags, s)
    s = sub('File List', dtree, s)
    return s
