#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

"""

"""


def get_var(m, vname, platform=None, arch=None):
    var = m.get(vname)
    if platform:
        pm = m.get('platform')
        if pm:
            var = get_var2(pm, vname, arch) or var

    return var


def get_var2(pm, vname, arch=None):
    var = pm.get(vname)
    if arch:
        var = pm.get(arch, {}).get(vname) or var

    return var


def split(l):
    def do(x):
        if x is None:
            return []
        else:
            return x.split(' ')

    if l is None:
        return []
    elif isinstance(l, list):
        return [xx for x in l for xx in do(x)]
    else:
        return do(l)


def split_all(*args):
    return [xx for x in args for xx in split(x)]


def get_list(l):
    if l is None:
        return []
    elif type(l) is list:
        return l
    else:
        return [l]


def get_list_all(*args):
    return [xx for x in args for xx in get_list(x)]


def bash_expand(s):
    start = s.find('{')
    end = s.find('}', start)
    if start == -1 or end == -1:
        if start != end:
            raise Exception('no matched brace in "{}"'.format(s))
        return [s]
    else:
        prefix = s[:start]
        suffix = s[end + 1:]
        mid = s[start + 1:end]
        return [prefix + x + suffix for x in mid.split(',')]


def bash_expand_list(l):
    if l is None:
        return []

    results = []
    for item in get_list(l):
        results += bash_expand(item)
    return results


def unix_path(l):
    return l.replace('\\', '/')


def quote(paths):
    def quote_s(path):
        return '"' + path + '"'

    if type(paths) is str:
        return quote_s(paths)
    else:
        return map(quote_s, paths)
