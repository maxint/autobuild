#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Build project w.r.t. meta build information.
"""

import os
import shutil
import sdkutil

import logging
log = logging.getLogger('build')


def prepare(binfos):
    import string

    plat = binfos['platform']
    arch = binfos['arch']
    sdk = binfos.get('sdk')
    objdir = ''

    bm = binfos['config']['build']

    def gen(vname):
        if vname in bm:
            return string.Template(bm.get(vname)).safe_substitute(
                platform=plat,
                platform_abbr=binfos['platform_abbr'],
                arch=arch,
                sdk=sdk,
                objdir=objdir)
        else:
            return None

    objdir = gen('objdir') or '_obj'
    binfos['objdir'] = objdir
    binfos['sdkdir'] = gen('sdkdir') or '.'
    binfos['libsubdir'] = gen('libsubdir')
    log.debug('objdir = ' + binfos['objdir'])
    log.debug('sdkdir = ' + binfos['sdkdir'])
    log.debug('libsubdir = ' + str(binfos['libsubdir']))
    sdkutil.mkdirs(binfos['objdir'])
    sdkutil.mkdirs(binfos['sdkdir'])


def get_lib_location(libdir, name):
    import glob
    libs = glob.glob(os.path.join(libdir, '*{}*'.format(name)))
    assert libs, 'Can not find library location'
    for path in libs:
        for patt in ['.dll', '.lib', '.so', '.a', '.dylib']:
            if path.endswith(patt):
                return path


def dist(binfos, dinfos):
    ''' Create SDK packages, return SDK paths.
    dinfos:
        name
        cflags:
        releasenote:
        outputdir:
        depends:
        dists: []
            name:
            incs: []
            docs: []
            samplecodes: []
    '''
    sdkzips = []
    for m in dinfos.get('dists', []):
        incs = m.get('incs')
        docs = m.get('docs')
        samplecodes = m.get('samplecodes')

        import makepkg
        zippath = makepkg.makepkg(
            binfos['platform_abbr'],
            dinfos['releasenote'],
            get_lib_location(dinfos['outputdir'], dinfos['name']),
            dinfos['cflags'],
            dinfos['depends'],
            incs,
            samplecodes,
            docs,
            os.path.join(binfos['objdir'], m['name']),
            binfos['libsubdir'])

        sdkutil.mkdirs(binfos['sdkdir'])
        dstpath = os.path.join(binfos['sdkdir'], os.path.basename(zippath))
        log.debug('Copy SDK to %s', dstpath)
        shutil.copyfile(zippath, dstpath)
        sdkzips.append(os.path.realpath(dstpath))

    return sdkzips


def build(binfos):
    prepare(binfos)
    objdir = binfos['objdir']

    import build_cmake
    dinfos = build_cmake.build(binfos, objdir)

    return dist(binfos, dinfos)
