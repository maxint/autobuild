#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Build all targets with cmake.
"""

import depends
import updatever
import cmakelists
import os
import parseutils as utils
import re
import glob
import sdkutil

import logging
log = logging.getLogger('build')


def get_programfiles():
    progdir = os.environ.get('ProgramFiles(x86)')
    if not progdir:
        progdir = os.environ.get('ProgramFiles')
    assert progdir, 'Can not find "ProgramFiles" of windows system'
    return progdir


def get_cmake_program():
    '''find cmake command path'''
    from sys import platform
    from binary import getpath
    if os.name == 'nt':
        paths = glob.glob('{}/*/bin/cmake.exe'.format(get_programfiles()))
        if not paths:
            paths = glob.glob(getpath('cmake-*-win32-x86/bin/cmake.exe'))
    elif platform == 'linux' or platform == 'linux2':
        paths = glob.glob(getpath('cmake-*-Linux-i386/bin/cmake'))
    elif platform == 'darwin':
        patt = 'cmake-*-Darwin*/CMake.app/Contents/bin/cmake'
        paths = glob.glob(getpath(patt))
    else:
        return 'cmake'

    assert paths, 'Can not find cmake'
    path = os.path.realpath(paths[0])
    return '"{}"'.format(path)


def get_cmake_generator(platform):
    '''find cmake generator'''
    if platform == 'windows':
        return '"NMake Makefiles"'
    else:
        return '"Unix Makefiles"'


def get_final_cmd(binfos, cmake):
    cmd = ''

    plat = binfos.get('platform')
    cm = binfos.get('cross', {})

    # cmake program
    cmd += '{} -G {} -DCMAKE_BUILD_TYPE={}'.format(
        cmake,
        get_cmake_generator(plat),
        'DEBUG' if binfos.get('debug') else 'RELEASE',
    )

    # for debug
    cmd += ' -DCMAKE_VERBOSE_MAKEFILE={}'.format(
        1 if binfos.get('verbose_make') else 0,
    )

    # no rpath
    cmd += ' -DCMAKE_SKIP_RPATH=1'

    # cmake parameters
    cmake_m = cm.get('cmake')
    if cmake_m:
        cmd += ' ' + ' '.join(utils.get_list(cmake_m))

    # make program
    cmd += ' -DCMAKE_MAKE_PROGRAM={0}'.format(
        cm.get('make') or binfos.get('make') or 'make'
    )

    return cm.get('run'), cmd


def isvalid_cmakecache(dpath):
    path = os.path.join(dpath, 'CMakeCache.txt')
    if os.path.exists(path):
        with open(path, 'rt') as fp:
            text = fp.read()
            m = re.search(r'CMAKE_C_COMPILER:FILEPATH=(.*)', text)
            if m is None:
                log.warn('Can not find C compiler in CMakeCache.txt')
            return m and os.path.exists(m.group(1))


def clean_cmakecache(objdir, level=4):
    ''' Remove all CMakeCache.txt in subdirectories'''
    for root, _, files in os.walk(objdir):
        if root.count('\\') + root.count('/') > level:
            continue
        for f in files:
            if f in ['CMakeCache.txt', 'Makefile', 'depend.make']:
                os.remove(os.path.join(root, f))


def get_cflags(objdir, libname):
    import glob
    path = '{}/lib/CMakeFiles/{}.dir/flags.make'.format(objdir, libname)
    g = glob.glob(path)
    assert g, 'Can not find "flags.make" file'
    with open(g[0], 'rt') as fp:
        text = fp.read()
        m = re.search(r'CX{0,2}_FLAGS = (.*)\n', text)
        assert m, 'Regex failed'
        return m.group(1)


def run(cmd):
    import subprocess
    log.debug(cmd)
    subprocess.check_call(cmd, shell=True)


def create_cmd_file(dpath, cmd):
    path = os.path.join(dpath, 'build.bat' if os.name == 'nt' else 'build.sh')
    if os.name == 'nt':
        cmd += '\npause'
    with open(path, 'wt') as f:
        f.write(cmd)
    return path


def build(binfos, objdir):
    dinfos = generate_build(binfos, binfos['project'])

    # check clean CMakeCache.txt
    if not isvalid_cmakecache(objdir):
        clean_cmakecache(objdir)

    cmake = binfos['config'].get('cmake') or get_cmake_program()
    pstr, cmd = get_final_cmd(binfos, cmake)
    build_cmd = '{} --build "{}"'.format(cmake, objdir)
    cmd = 'cd "{}" && '.format(objdir) + cmd
    if pstr:
        cmd = pstr + ' && ' + cmd
        build_cmd = pstr + ' && ' + build_cmd

    create_cmd_file(objdir, build_cmd)

    log.debug('Runing make to ' + objdir)
    run(cmd)

    # find location of library
    dinfos['outputdir'] = objdir + '/lib'
    dinfos['cflags'] = get_cflags(objdir, dinfos['name'])

    run(build_cmd)

    return dinfos


def generate_build(binfos, pm):
    def get_flags(vname):
        return pm.get(vname) or ''

    def get_list(vname):
        return pm.get(vname) or []

    cflags = get_flags('cflags')
    cppflags = get_flags('cppflags')
    ldflags = get_flags('ldflags')
    macros = get_list('macros')
    deps = pm.get('depends')

    name = pm.get('name')
    btype = pm.get('type')
    inc_dirs = pm.get('inc_dirs')
    srcs = pm.get('srcs')

    dep_incs, dep_libs = depends.get_incs_libs(deps)
    sys_libs = pm.get('system_libs', [])

    def local_path(subdir):
        return os.path.join(binfos['objdir'], subdir)

    # generate version file
    libdir = os.path.join(binfos['objdir'], 'lib')
    sdkutil.mkdirs(libdir)
    gen_verfile = updatever.replace2(binfos,
                                     libdir,
                                     pm['releasenote'],
                                     pm['version_file'])
    srcs[-1].append(gen_verfile)

    # remove old build libraries
    map(os.remove, sdkutil.glob_libraries(libdir))

    # library
    generate_target(binfos, 'lib',
                    name=name,
                    btype=btype,
                    srcs=srcs,
                    macros=macros,
                    cflags=cflags,
                    cppflags=cppflags,
                    ldflags=ldflags,
                    inc_dirs=inc_dirs + dep_incs,
                    libs=(dep_libs or []) + sys_libs,
                    )

    clist = cmakelists.CMakeLists(name)
    clist.subdir('lib')

    prefix = name + '_'

    # sample code
    for x in pm.get('dists', []):
        tname = x['name']
        clist.subdir(x['name'])
        sdkutil.mkdirs(local_path(tname))
        dincs = map(os.path.dirname, utils.get_list(x.get('incs')))
        generate_target(binfos, tname,
                        name=prefix + tname,
                        btype='exe',
                        inc_dirs=dincs + dep_incs,
                        srcs=x.get('samplecodes'),
                        cflags=cflags,
                        cppflags=cppflags,
                        ldflags=ldflags,
                        libs=[name] + sys_libs)

    # tests
    for x in pm.get('tests', []):
        tname = x['name']
        clist.subdir(x['name'])
        sdkutil.mkdirs(local_path(tname))
        generate_target(binfos, tname,
                        name=prefix + tname,
                        btype='exe',
                        inc_dirs=x.get('inc_dirs') + dep_incs,
                        srcs=x.get('srcs'),
                        cflags=cflags,
                        cppflags=cppflags,
                        ldflags=ldflags,
                        libs=[name] + sys_libs)

    clist.write(binfos['objdir'])
    log.debug('Creating "%s/CMakeLists.txt"', binfos['objdir'])

    return dict(
        name=name,
        releasenote=pm['releasenote'],
        libdir=libdir,
        depends=deps,
        dists=pm.get('dists', [])
    )


def generate_target(binfos, subdir, **vargs):
    target_dir = os.path.join(binfos['objdir'], subdir)
    log.debug('Creating "%s/CMakeLists.txt"', target_dir)
    cmakelists.write2(
        target_dir,
        **vargs)
