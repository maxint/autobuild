#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Generate makefile w.r.t. project config file (e.g. project.yaml).
'''

import os

import logging
log = logging.getLogger('build')


def log_to_stdout(log):
    import sys
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    fmt = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(fmt)
    log.addHandler(ch)


def print_dict(d):
    import pprint
    pprint.PrettyPrinter().pprint(d)


def build(fname, **binfos):
    ''' Build ArcSoft library SDK
        return SDK library path
    '''
    import buildinfo
    import config

    if binfos.get('verbose'):
        log_to_stdout(log)

    if binfos.get('ipdb'):
        enable_ipdb_excepthook()

    # load config to binfos['config']
    binfos.update(config.load())

    # fill arch and sdk
    config.fetch_defaults(binfos, ['arch', 'sdk', 'make', 'compiler'])
    # fill features
    config.fetch_available_features(binfos)

    # remove empty items
    buildinfo.remove_empty(binfos)

    fname = os.path.realpath(fname)

    # change to project directory
    cdir = os.path.dirname(fname)
    odir = os.getcwd()
    os.chdir(cdir)

    try:
        # load project.yaml
        import project
        project.load(binfos, fname)

        pd = binfos['project']

        KEYS = ['cflags',
                'cppflags',
                'ldflags',
                'macros',
                'depends',
                'system_libs',
                'enabled',
                'type'
                ]
        overwrite(src=binfos, dst=pd, keys=KEYS)

        # fill other defaults
        config.fetch_defaults(binfos,
                              ['cflags', 'cppflags', 'enabled', 'ldflags'],
                              pd)

        # parse enabled
        config.fill_features(binfos, pd.get('enabled'), pd)

        # prepare sdk
        sdk_options = config.prepare_sdk(binfos)
        merge(src=sdk_options, dst=pd)

        # fill other info
        buildinfo.fill_infos(binfos, binfos['project'].get('cflags'))

        # parse depends
        import depends
        pd['depends'] = depends.get(binfos, pd.get('depends'))

        # build
        import build
        return build.build(binfos)
    finally:
        # restore working directory
        os.chdir(odir)

    print 'Done!'

    return None


def parse_platform_abbr(plat, arch):
    assert plat, 'No platform is specified'
    if plat == 'win32':
        plat = 'windows'
        arch = 'x86'
    elif plat == 'win64':
        plat = 'windows'
        arch = 'x64'
    elif plat == 'android64':
        plat = 'android'
        arch = 'arm64'

    if arch and args.arch and arch != args.arch:
        raise Exception('"{}" conflicts with "{}"'.format(args.platform,
                                                          args.arch))
    return plat, arch


def parse_args(args):
    plat, arch = parse_platform_abbr(args.platform, args.arch)

    args.platform = plat
    args.arch = arch
    binfos = vars(args)

    # parse extra
    if args.extra:
        import urlparse
        d = urlparse.parse_qs(args.extra)
        for k, v in d.iteritems():
            if k in ['type', 'sdk']:
                assert len(v) == 1
                binfos[k] = v[0]
            else:
                binfos[k] = v

    for k in ['extra', 'projfile']:
        binfos.pop(k)

    return binfos


def overwrite(src, dst, keys):
    for k in keys:
        if not src.get(k) is None:
            dst[k] = src[k]


def merge(src, dst):
    from parseutils import get_list
    for k, v in src.iteritems():
        dst[k] = dst.get(k, []) + get_list(v)


def enable_ipdb_excepthook():
    '''Call ipython when raising exception'''
    try:
        from IPython.core import ultratb
        import sys
        sys.excepthook = ultratb.FormattedTB(mode='Verbose',
                                             color_scheme='Linux',
                                             call_pdb=1)
    except:
        log.warning('Please install IPython to enable IPython except hook')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Auto build system')

    parser.add_argument('--platform', '-p', nargs='?', default='android',
                        help='target platform (e.g. windows/ios)')
    parser.add_argument('--arch', '-a', nargs='?', default=None,
                        help='target architecture (e.g. arm/arm64/x86)')
    parser.add_argument('--cflags', nargs='?', default=None,
                        help='c compiler flags')
    parser.add_argument('--cppflags', nargs='?', default=None,
                        help='c++ compiler flags')
    parser.add_argument('--ldflags', nargs='?', default=None,
                        help='linker flags')
    parser.add_argument('--macros', nargs='?', default=None,
                        help='compiler defines (e.g. ANDROID=1)')
    parser.add_argument('--extra', '-e', nargs='?', default=None,
                        help='extra parameters')
    parser.add_argument('--verbose', '-V', action='store_true',
                        help='more auto build information')
    parser.add_argument('--verbose_make', '-V1', action='store_true',
                        help='more make information')
    parser.add_argument('--debug', action='store_true',
                        help='build library in debug mode')
    parser.add_argument('--ipdb', '-D', action='store_true',
                        help='handle exception by IPython')
    parser.add_argument('projfile', nargs='?', default='project.yaml',
                        help='input project file (e.g. project.yaml)')

    args = parser.parse_args()

    build(args.projfile, **parse_args(args))
