# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

# cross compiling setup
set(CMAKE_SYSTEM_NAME Darwin)
set(CMAKE_SYSTEM_PROCESSOR arm) # optional
set(CMAKE_CROSSCOMPILING TRUE)

# platform flags
set(APPLE 1)
set(IOS 1)

# select xcode version and set OSX_DEVELOPER_ROOT
if(NOT OSX_DEVELOPER_ROOT)
	if(DEFINED ENV{OSX_DEVELOPER_ROOT})
		set(OSX_DEVELOPER_ROOT "$ENV{OSX_DEVELOPER_ROOT}")
  else()
    find_program(CMAKE_XCODE_SELECT xcode-select)
    if(CMAKE_XCODE_SELECT)
      execute_process(COMMAND ${CMAKE_XCODE_SELECT} "-print-path"
        OUTPUT_VARIABLE OSX_DEVELOPER_ROOT OUTPUT_STRIP_TRAILING_WHITESPACE)
    endif()
	endif()
	set(OSX_DEVELOPER_ROOT "${OSX_DEVELOPER_ROOT}" CACHE PATH "OSX Developer toolchain location")
endif()

# hard set values
set(IOS_SDK_VERSION "7.1")
set(IOS_TARGET "iPhoneOS")
set(IOS_ARCH "armv7")

# some internal values
set(IOS_DEVELOPER_ROOT "${OSX_DEVELOPER_ROOT}/Platforms/${IOS_TARGET}.platform/Developer")
set(IOS_SDK_ROOT "${IOS_DEVELOPER_ROOT}/SDKs/${IOS_TARGET}${IOS_SDK_VERSION}.sdk")
set(OSX_TOOLCHAIN_ROOT "${OSX_DEVELOPER_ROOT}/Toolchains/XcodeDefault.xctoolchain")

# root path
set(CMAKE_SYSTEM_PROGRAM_PATH "${OSX_TOOLCHAIN_ROOT}/usr")
set(CMAKE_FIND_ROOT_PATH ${OSX_TOOLCHAIN_ROOT} ${IOS_DEVELOPER_ROOT} ${IOS_SDK_ROOT})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# compiler
find_program(CMAKE_C_COMPILER clang)
find_program(CMAKE_CXX_COMPILER clang++)
set(CMAKE_C_COMPILER_ID Clang)
set(CMAKE_CXX_COMPILER_ID Clang)

# set compiler flags
set(CMAKE_C_FLAGS "--sysroot=${IOS_SDK_ROOT} -miphoneos-version-min=${IOS_SDK_VERSION} -arch ${IOS_ARCH} -v" CACHE STRING "C flags" FORCE)
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "C++ flags" FORCE)

# vim: expandtab filetype=cmake shiftwidth=2 tabstop=2 softtabstop=2
