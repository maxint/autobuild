# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

# get SDK root
if(NOT TIZEN_SDK_ROOT)
  if(DEFINED ENV{TIZEN_SDK_ROOT})
    set(TIZEN_SDK_ROOT $ENV{TIZEN_SDK_ROOT})
  endif()
  set(TIZEN_SDK_ROOT "${TIZEN_SDK_ROOT}" CACHE PATH "Tizen toolchain location")
endif()

# basic setup
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm) # optional
set(CMAKE_CROSSCOMPILING TRUE)

# platform flags
set(TIZEN 1)

# SDK related directories
set(TIZEN_GCC_TOOLCHAIN ${TIZEN_SDK_ROOT}/tools/arm-linux-gnueabi-gcc-4.5)
set(CMAKE_SYSROOT ${TIZEN_SDK_ROOT}/platforms/tizen2.2/rootstraps/tizen-device-2.2.native) 
set(TIZEN_CXX_DIR ${CMAKE_SYSROOT}/usr/include/c++/4.5.3)

# compilers
find_program(CMAKE_C_COMPILER clang PATHS "${TIZEN_SDK_ROOT}/tools/llvm-3.1/bin")
find_program(CMAKE_CXX_COMPILER clang++ PATHS "${TIZEN_SDK_ROOT}/tools/llvm-3.1/bin")
find_program(CMAKE_AR ar PATH "${TIZEN_GCC_TOOLCHAIN}/arm-linux-gnueabi/bin")
set(CMAKE_C_COMPILER_ID Clang)
set(CMAKE_CXX_COMPILER_ID Clang)

# compiler and linker flags
set(CMAKE_C_FLAGS "-sysroot=${CMAKE_SYSROOT} -target arm-tizen-linux-gnueabi -gcc-toolchain ${TIZEN_GCC_TOOLCHAIN} -I${TIZEN_CXX_DIR} -I${TIZEN_CXX_DIR}/armv7l-tizen-linux-gnueabi " CACHE STRING "C Flags" FORCE)
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "C++ Flags" FORCE)
set(CMAKE_LINKER_FLAGS "-Wl,--no-undefined -lc -lm -lstdc++ -lgcc -ldl -lrt" CACHE STRING "Linker Flags" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}" CACHE STRING "" FORCE)
set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}" CACHE STRING "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_LINKER_FLAGS}" CACHE STRING "" FORCE)

# NOTE: (optional) do not contribute to find compiler program, e.g. ar
#set(CMAKE_SYSROOT ${CMAKE_SYSROOT})
#set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

# vim: expandtab filetype=cmake shiftwidth=2 tabstop=2 softtabstop=2
