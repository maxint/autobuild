#
# Android NDK toolchain file for CMake
#
# Note: this version targets NDK r8, r9, r10
#
# need to know where the NDK resides, enviroment variable ANDROID_NDK_ROOT
#
#   ANDROID_API=android-9
#
#   ANDROID_ABI=armeabi-v7a
#
#     Posible values are:
#       armebi
#       armeabi-v6
#       armeabi-v7a
#       arm64-v8a
#       x86
#       x86_64
#       mips
#       mips64
#
#   ANDROID_TOOLCHAIN=arm-linux-androideabi-4.7
#
#   ANDROID_STL=system - specify the runtime to use
#
#     Posible values are:
#       system
#       gabi++_static
#       gabi++_shared
#       gnustl_static
#       gnustl_shared
#       stlport_static
#       stlport_shared
#

# get NDK root
if(NOT ANDROID_NDK_ROOT)
  if(DEFINED ENV{ANDROID_NDK_ROOT})
    message(WARNING "Using enviroment variable ANDROID_NDK_ROOT")
    set(ANDROID_NDK_ROOT "$ENV{ANDROID_NDK_ROOT}")
  else()
    message(FATAL_ERROR "ANDROID_NDK_ROOT does not defined")
  endif()
endif()
set(ANDROID_NDK_ROOT "${ANDROID_NDK_ROOT}" CACHE PATH "Android Toolchain location" FORCE)

# basic setup
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_CROSSCOMPILING TRUE)

# for convenience
set(ANDROID TRUE)

# platforms
file(GLOB ANDROID_API_SUPPORTED RELATIVE ${ANDROID_NDK_ROOT}/platforms "${ANDROID_NDK_ROOT}/platforms/android-*")
if(NOT ANDROID_API)
  set(ANDROID_API "android-9")
  list(FIND ANDROID_API_SUPPORTED ${ANDROID_API} ANDROID_API_FOUND)
  if(ANDROID_API_FOUND EQUAL -1)
    list(GET ANDROID_API_SUPPORTED -1 ANDROID_API)
  endif()
endif()
set(ANDROID_API "${ANDROID_API}" CACHE STRING "Android SDK API (${ANDROID_API_SUPPORTED})" FORCE)
set_property(CACHE ANDROID_API PROPERTY STRINGS ${ANDROID_API_SUPPORTED})
set(ANDROID_API_ROOT ${ANDROID_NDK_ROOT}/platforms/${ANDROID_API})

# ABI
set(_lib_root "${ANDROID_NDK_ROOT}/sources/cxx-stl/stlport/libs")
file(GLOB ANDROID_ABI_SUPPORTED RELATIVE "${_lib_root}" "${_lib_root}/*")
if(NOT ANDROID_ABI)
  set(ANDROID_ABI "armeabi-v7a")
endif()
list(FIND ANDROID_ABI_SUPPORTED ${ANDROID_ABI} ANDROID_ABI_FOUND)
if(ANDROID_ABI_FOUND EQUAL -1)
  message(WARNING "ANDROID_ABI (${ANDROID_ABI}) is not supported. Rollback to 'armeabi'")
  set(ANDROID_ABI "armeabi")
endif()
set(ANDROID_ABI "${ANDROID_ABI}" CACHE STRING "The target ABI for Android. If arm, then armeabi-v7a is recommended for hardware floating point" FORCE)
set_property(CACHE ANDROID_ABI PROPERTY STRINGS ${ANDROID_ABI_SUPPORTED})

# system info
string(TOUPPER ${ANDROID_API} CMAKE_SYSTEM_VERSION)
string(TOUPPER ${ANDROID_ABI} CMAKE_SYSTEM_PROCESSOR)

# set target ABI options
set(ANDROID_ARCH ${ANDROID_ABI})
if(ANDROID_ABI STREQUAL "x86")
elseif(ANDROID_ABI STREQUAL "x86_64")
elseif(ANDROID_ABI STREQUAL "mips")
elseif(ANDROID_ABI STREQUAL "mips64")
elseif(ANDROID_ABI STREQUAL "armeabi")
  set(ANDROID_ARCH "arm")
  set(ANDROID_C_FLAGS "-march=armv5te -mtune=xscale -msoft-float")
elseif(ANDROID_ABI STREQUAL "armeabi-v6")
  set(ANDROID_ARCH "arm")
  set(ANDROID_C_FLAGS "-march=armv6 -mfloat-abi=softfp -mfpu=vfp")
elseif(ANDROID_ABI STREQUAL "armeabi-v7a")
  set(ANDROID_ARCH "arm")
  set(ANDROID_C_FLAGS "-march=armv7-a -mfloat-abi=softfp -mfpu=neon")
elseif(ANDROID_ABI STREQUAL "arm64-v8a")
  set(ANDROID_ARCH "arm64")
  set(ANDROID_C_FLAGS "-march=armv8-a")
endif()

# sysroot - in Android this in function of Android API and architecture
set(CMAKE_SYSROOT "${ANDROID_API_ROOT}/arch-${ANDROID_ARCH}")

# toolchain
if("${ANDROID_ARCH}" STREQUAL "arm64")
	set(ANDROID_ARCH_PREFIX aarch64)
else()
	set(ANDROID_ARCH_PREFIX ${ANDROID_ARCH})
endif()
file(GLOB ANDROID_TOOLCHAIN_SUPPORTED RELATIVE "${ANDROID_NDK_ROOT}/toolchains" "${ANDROID_NDK_ROOT}/toolchains/${ANDROID_ARCH_PREFIX}*-4.*")
list(SORT ANDROID_TOOLCHAIN_SUPPORTED)
if(NOT ANDROID_TOOLCHAIN)
  list(GET ANDROID_TOOLCHAIN_SUPPORTED -1 ANDROID_TOOLCHAIN)
endif()
set(ANDROID_TOOLCHAIN "${ANDROID_TOOLCHAIN}" CACHE STRING "Android toolchains" FORCE)
set_property(CACHE ANDROID_TOOLCHAIN PROPERTY STRINGS ${ANDROID_TOOLCHAIN_SUPPORTED})
file(GLOB ANDROID_TOOLCHAIN_ROOT "${ANDROID_NDK_ROOT}/toolchains/${ANDROID_TOOLCHAIN}/prebuilt/*")

# get arch, ABI and gcc version
string(REGEX MATCH "([.0-9]+)$" ANDROID_COMPILER_VERSION "${ANDROID_TOOLCHAIN}")

# STL
set(ANDROID_STL_SUPPORTED "system;gabi++_static;gabi++_shared;gnustl_static;gnustl_shared;stlport_static;stlport_shared")
if(NOT ANDROID_STL)
  set(ANDROID_STL "system")
endif()
set(ANDROID_STL "${ANDROID_STL}" CACHE STRING "C++ runtime" FORCE)
set_property(CACHE ANDROID_STL PROPERTY STRINGS ${ANDROID_STL_SUPPORTED})

set(ANDROID_STL_ROOT "${ANDROID_NDK_ROOT}/sources/cxx-stl")
if(ANDROID_STL STREQUAL "system")
  set(ANDROID_RTTI             OFF)
  set(ANDROID_EXCEPTIONS       OFF)
	set(ANDROID_STL_ROOT         "${ANDROID_STL_ROOT}/system")
  set(ANDROID_STL_INCLUDE_DIRS "${ANDROID_STL_ROOT}/include")
elseif(ANDROID_STL MATCHES "gabi")
  set(ANDROID_RTTI             ON)
  set(ANDROID_EXCEPTIONS       ON)
	set(ANDROID_STL_ROOT         "${ANDROID_STL_ROOT}/gabi++")
  set(ANDROID_STL_INCLUDE_DIRS "${ANDROID_STL_ROOT}/include")
  set(ANDROID_STL_LDFLAGS      "-L${ANDROID_STL_ROOT}/libs/${ANDROID_ABI} -lgabi++_static")
elseif(ANDROID_STL MATCHES "stlport") 
  set(ANDROID_RTTI             ON)
  set(ANDROID_EXCEPTIONS       ON)
	set(ANDROID_STL_ROOT         "${ANDROID_STL_ROOT}/stlport")
  set(ANDROID_STL_INCLUDE_DIRS "${ANDROID_STL_ROOT}/stlport")
  set(ANDROID_STL_LDFLAGS      "-L${ANDROID_STL_ROOT}/libs/${ANDROID_ABI} -lstlport_static")
elseif(ANDROID_STL MATCHES "gnustl")
  set(ANDROID_RTTI             ON)
  set(ANDROID_EXCEPTIONS       ON)
	set(ANDROID_STL_ROOT         "${ANDROID_STL_ROOT}/gnu-libstdc++/${ANDROID_COMPILER_VERSION}")
  set(ANDROID_STL_INCLUDE_DIRS "${ANDROID_STL_ROOT}/include" "${ANDROID_STL_ROOT}/libs/${ANDROID_ABI}/include")
  set(ANDROID_STL_LDFLAGS      "-L${ANDROID_STL_ROOT}/libs/${ANDROID_ABI} -lgnustl_static -lsupc++")
endif()
# case of shared STL linkage
if(ANDROID_STL MATCHES "shared" AND DEFINED ANDROID_STL_LDFLAGS)
  string(REPLACE "_static" "_shared" ANDROID_STL_LDFLAGS "${ANDROID_STL_LDFLAGS}")
  # TODO: copy .so
endif()
# NOTE: set -fno-exceptions -fno-rtti when use system
if(NOT ANDROID_RTTI)
  set(ANDROID_C_FLAGS "${ANDROID_C_FLAGS} -fno-rtti")
endif()
if(NOT ANDROID_EXCEPTIONS)
  set(ANDROID_C_FLAGS "${ANDROID_C_FLAGS} -fno-exceptions")
endif()

# search paths
set(CMAKE_FIND_ROOT_PATH "${ANDROID_TOOLCHAIN_ROOT}/bin" "${CMAKE_SYSROOT}")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# compilers (set CMAKE_C_COMPILER_ID and CMAKE_CXX_COMPILER automatically)
if(CMAKE_HOST_WIN32)
  set(TOOL_OS_SUFFIX ".exe")
endif()
file(GLOB CMAKE_C_COMPILER   "${ANDROID_TOOLCHAIN_ROOT}/bin/*-gcc${TOOL_OS_SUFFIX}")
file(GLOB CMAKE_CXX_COMPILER "${ANDROID_TOOLCHAIN_ROOT}/bin/*-g++${TOOL_OS_SUFFIX}")
# NOTE: fix bug of no -D* passed when checking compilers
include(CMakeForceCompiler)
cmake_force_c_compiler(${CMAKE_C_COMPILER} GNU)
cmake_force_cxx_compiler(${CMAKE_CXX_COMPILER} GNU)

# get the gcc companion library
exec_program(${CMAKE_C_COMPILER} ARGS "-print-libgcc-file-name" OUTPUT_VARIABLE ANDROID_LIBGCC)

# global includes and link directories
include_directories(SYSTEM ${ANDROID_STL_INCLUDE_DIRS})

# cflags, cppflags, ldflags
# NOTE: -nostdlib causes link error when compiling 'viv': hidden symbol `__dso_handle'
# set sysroot manually for low version cmake
set(CMAKE_C_FLAGS "${ANDROID_C_FLAGS}" CACHE STRING "C Flags" FORCE)
#set(CMAKE_C_FLAGS "--sysroot=${CMAKE_SYSROOT} ${CMAKE_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "C++ Flags" FORCE)
set(CMAKE_LINKER_FLAGS "${ANDROID_LIBGCC} ${ANDROID_STL_LDFLAGS} -lc -lm -lstdc++ -ldl -llog" CACHE STRING "Shared Linker Flags" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}" CACHE STRING "" FORCE)
set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_LINKER_FLAGS}" CACHE STRING "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS	  "${CMAKE_LINKER_FLAGS}" CACHE STRING "" FORCE)
