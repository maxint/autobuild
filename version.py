#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Get version number from files.
'''

import re


def split3(verstr):
    '''Get splited version number from version string, e.g.(0.1.1)'''
    m = re.search(r"^(\d+).(\d+).(\d+)$", verstr)
    gs = m.groups()
    return gs[0], gs[1], gs[2]


def split4(verstr):
    '''Get splited version number from version string, e.g.(0.1.xxxx.1)'''
    #e.g. ArcSoft_VdoInVideo_0.1.12018.125_log(dev Nov 17 2013 14:08:30)
    m = re.search(r'(?:_|\b)(\d{1,3})\.(\d{1,3})\.(\d{1,5})\.(\d{1,4})(?:_|\b)', verstr).groups()
    return m[0], m[1], m[2], m[3]


def get_full_version_number(filename):
    '''Get full version number from library binary file'''
    text = open(filename, 'rb').read()
    #import ipdb; ipdb.set_trace()
    return re.search(r'ArcSoft_[a-zA-z_]+_\d{1,3}\.\d{1,3}\.\d{1,5}\.\d{1,4}[_a-z]*', text).group(0)


def get_version_string(filename):
    '''Get full version number with date from library binary file'''
    text = open(filename, 'rb').read()
    return re.search(r'ArcSoft_[a-zA-z_]+_\d{1,3}\.\d{1,3}\.\d{1,5}\.\d{1,4}[_a-zA-Z0-9:\(\)\ ]*', text).group(0)


def get_version_numbers(filename, full=False):
    '''Get splited version number from file'''
    if full:
        text = get_version_string(filename)
    else:
        text = open(filename, 'rb').read()
    return split4(text)


def get_version_number(filename, full=False):
    '''Get version number from file'''
    return '.'.join(get_version_numbers(filename, full))


def get_lite_version_number(filename, full=False):
    '''Get version number without platform number from file'''
    vers = get_version_numbers(filename, full)
    return '.'.join([vers[0], vers[1], vers[3]])

if __name__ == '__main__':
    import sys
    if len(sys.argv) != 2:
        print 'usage: version.py [version file]'
    else:
        print get_version_string(sys.argv[1])
