#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Get binary path of auto build system.
"""

import os


def getpath(subpath):
    curdir = os.path.dirname(__file__)
    return os.path.join(curdir, 'binary', subpath)
