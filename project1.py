#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

"""
Parse project.yaml (version 1)
"""

import os
import glob
import parseutils as utils

import logging
log = logging.getLogger('build')


def load(binfos, m):
    authors = m.get('authors')
    assert authors, 'Please specify the authors of this project'

    log.debug('Authors: {}'.format(authors))
    d = parse_build(binfos, m.get('build') or m)
    d['authors'] = authors
    return d


def parse_build(binfos, m):
    d = parse_target(binfos, m, btype='static')

    # copy keys
    for k in ['version_file', 'releasenote']:
        d[k] = m[k]

    dnames = filter(lambda x: x.startswith('dist'), m.keys())
    tnames = filter(lambda x: x.startswith('test'), m.keys())
    d['dists'] = [parse_dist(m.get(k), k) for k in dnames]
    d['tests'] = [parse_test(binfos, m.get(k), k) for k in tnames]

    return d


def parse_dist(m, dname):
    d = m.copy()
    d.setdefault('name', dname)
    return d


def parse_test(binfos, m, tname):
    d = parse_target(binfos, m, tname, 'exe')
    return d


def parse_target(binfos, m, name=None, btype=None):
    '''
        name:
        type:
        inc_dirs: []
        srcs: [[]]
        cflags:
        cppflags:
        ldflags:
        macros: []
        depends: []
        enabled: []
    '''
    plat = binfos['platform']
    arch = binfos['arch']
    pm = m.get(plat, {})

    def get_all(vname):
        return [m.get(vname),
                m.get(arch, {}).get(vname),
                pm.get(vname),
                pm.get(arch, {}).get(vname)]

    def get_flags(vname):
        return ' '.join(utils.get_list(utils.get_var2(pm, vname, arch)))

    cflags = get_flags('cflags')
    cppflags = get_flags('cppflags')
    ldflags = get_flags('ldflags')
    depends = utils.get_list(m.get('depend_on') or m.get('depends'))
    macros = utils.split_all(get_all('macros'))
    enabled = utils.split_all(get_all('enabled'))
    inc_dirs, srcs = get_incs_srcs(m)

    d = dict(
        name=m.get('name') or name,
        type=utils.get_var(m, 'type', plat, arch) or btype,
        inc_dirs=inc_dirs,
        srcs=srcs,
        cflags=cflags,
        cppflags=cppflags,
        ldflags=ldflags,
        macros=macros,
        depends=depends,
        enabled=enabled
    )

    # remove empty items
    d = dict((k, v) for k, v in d.iteritems() if v)

    assert d['type'] in ['static', 'shared', 'exe'], 'Invalid build type'
    assert isinstance(d['name'], str), 'Invalid target name'

    return d


def exclude_sources(alist, excludes):
    if not excludes:
        return alist

    def match_exclude(x):
        for e in excludes:
            if x.endswith(e):
                return True
        return False

    return filter(lambda x: not match_exclude(x), alist)


def get_incs_srcs(m, excludes=None):
    ''' inc_dirs: [], srcs: [[]]'''
    inc_dirs = utils.bash_expand_list(m.get('inc_dirs'))
    src_dirs = utils.bash_expand_list(m.get('src_dirs'))

    # exclude
    excludes = utils.get_list_all(m.get('version_file'),
                                  m.get('excludes'))
    srcs = []

    def append_to_srcs(alist):
        if alist:
            # convert to unix path for excluding patterns
            if type(alist) is list:
                paths = map(utils.unix_path, alist)
            else:
                paths = utils.unix_path(alist)
            srcs.append(exclude_sources(paths, excludes))

    append_to_srcs(utils.get_list(m.get('srcs')))
    for sdir in src_dirs:
        if os.path.isdir(sdir):
            for t in ['*.cpp', '*.c', '*.cc']:
                append_to_srcs(glob.glob(os.path.join(sdir, t)))
        else:
            raise Exception('"{}" is not a directory'.format(sdir))

    return inc_dirs, srcs
