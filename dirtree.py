#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Generate directory tree for release notes.
'''

import os


COMMENTS = {
    '.pdf': 'Developer\'s guide',
    '.h': 'Head file',
    '.hpp': 'Head file',
    '.a': 'Library',
    '.so': 'Library',
    '.lib': 'Library',
    '.dll': 'Library',
    '.c': 'Sample codes',
    '.cpp': 'Sample codes',
}


def list_files(startpath, depth=0, comments=COMMENTS):
    contents = os.listdir(startpath)
    dirs = [x for x in contents if os.path.isdir(startpath + os.sep + x)]
    files = [x for x in contents if os.path.isfile(startpath + os.sep + x)]
    tstr = ''
    dirs.sort()
    files.sort()
    for item in dirs + files:
        path = startpath + os.sep + item
        msg = '|{}---{}'.format('   |' * depth, item)
        if os.path.isdir(path):
            tstr += msg + '\n'
            tstr += list_files(path, depth + 1)
        elif os.path.isfile(path):
            _, ext = os.path.splitext(path)
            if ext in comments:
                msg += ' ' * max(2, 50 - len(msg)) + comments[ext]
            tstr += msg + '\n'

    return tstr

if __name__ == '__main__':
    import sys
    startpath = '.'
    if len(sys.argv) == 2:
        startpath = sys.argv[1]
    print list_files(startpath)
