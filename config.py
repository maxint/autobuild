#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

import socket
import os
import sys
import logging
import string

try:
    import yaml
except:
    import binary
    sys.path.insert(0, binary.getpath('yaml.zip'))
    import yaml

log = logging.getLogger('build')

# Loading {{{


def load_yaml(fname):
    if os.path.exists(fname):
        with open(fname, 'rt') as fp:
            return yaml.safe_load(fp)


def print_dict(d):
    import pprint
    pprint.PrettyPrinter().pprint(d)


def get_nested(d, path):
    parts = os.path.split(path)
    for p in parts:
        if p:
            d = d.setdefault(p, {})
    return d


def load():
    cdir = os.path.dirname(__file__)
    odir = os.getcwd()
    os.chdir(cdir)

    d = dict()
    # load common
    for root, dirs, files in os.walk('config', True):
        if 'this.yaml' in files:
            pd = get_nested(d, os.path.dirname(root))
            pd[os.path.basename(root)] = load_common(root)
            files = filter(lambda x: not x.startswith('this'), files)
        dd = get_nested(d, root)
        for name in files:
            if not name.startswith('.'):
                key = os.path.splitext(name)[0]
                dd[key] = load_yaml(os.path.join(root, name))

    # load custom
    for root, dirs, files in os.walk('config', True):
        if 'this.yaml' in files:
            pd = get_nested(d, os.path.dirname(root))
            key = os.path.basename(root)
            pd[key] = merge(pd.get(key), load_custom(root))

    os.chdir(odir)

    return d


def load_common(root):
    return load_yaml(os.path.join(root, 'this.yaml'))


def load_custom(root):
    path = os.path.join(root, 'this.{}.yaml'.format(socket.gethostname()))
    return load_yaml(path)


def merge(d1, d2):
    if d1 is None:
        return d2
    if d2 is None:
        return d1
    if isinstance(d1, dict) and isinstance(d2, dict):
        keys = set(d1.iterkeys()) | set(d2.iterkeys())
        return dict((key, merge(d1.get(key), d2.get(key))) for key in keys)
    else:
        # d2 overwrite d1
        return d2


def load_with_custom(root):
    local_cfg = os.path.join(root, 'this-{}.yaml'.format(socket.gethostname()))
    local_d = load_yaml(local_cfg)
    common_d = load_yaml(os.path.join(root, 'this.yaml'))
    if local_d:
        return merge(common_d, local_d)
    else:
        return common_d


# Functions {{{

def get_first(m):
    return m.iterkeys().next() if m and len(m) == 1 else None


def get_native_sdk_root(m, ver=None):
    mm = m.get('sdk')
    ver = ver or get_first(mm)
    return mm.get(ver).get('root')


def get_variable(m, vname, arch=None, sdk=None):
    ''' Get the closest variable w.r.t. arch and sdk'''
    if m is None:
        return

    # platform
    var = m.get(vname)

    # platform > arch
    if arch:
        var = m.get(arch, {}).get(vname) or var

    mm = m.get('sdk')
    sdk = sdk or get_first(mm)
    if sdk and mm:
        sdk_m = mm[sdk]
        # platform > sdk
        var = sdk_m.get(vname) or var
        # platform > sdk > arch
        var = sdk_m.get(arch, {}).get(vname) or var

    return var


def get_path(m, platform, arch=None, sdk=None):
    m = m.get(platform)
    assert m, 'Unsupported platform "{}"'.format(platform)

    root = m.get('path', '')
    if root:
        assert 'root' not in m, 'Set "root" and "path" simultaneously in ' + m

    # platform > sdk
    mm = m.get('sdk')
    sdk = sdk or get_first(mm)
    if sdk and mm and sdk in mm:
        parts = list()
        mm = mm[sdk]
        # platform > sdk
        parts.append(mm.get('path', ''))
        # platform > sdk > arch
        mm = mm.get(arch)
        parts.append(mm.get('path', ''))
        subpath = os.path.join(*parts)
        if subpath:
            return os.path.join(root, subpath)

    # platform > arch
    if arch and arch in m and 'path' in m[arch]:
        return os.path.join(root, m[arch].get('path'))

    # platform
    return root


def get_full_path(m, platform, arch=None, sdk=None):
    m = m.get(platform)
    assert m, 'Unsupported platform "{}"'.format(platform)

    # platform
    root = m.get('root', '')
    return os.psth.join(root, get_path(m, platform, arch, sdk))


def get_cross_cmd(m, ver, arch):
    def cvt_arch(arch):
        if arch == 'x64':
            if sys.maxsize > 2 ** 32:
                return 'amd64'
            else:
                return 'x86_amd64'
        return arch

    ver = ver or get_first(m)
    return m.get('vc').get(ver).get(cvt_arch(arch))


def fill_default(target, defaults, patts=None):
    if not defaults:
        return

    for k, v in defaults.iteritems():
        if patts and k not in patts:
            continue
        if target.get(k) is None and not isinstance(v, dict):
            log.debug('Update "{}" to default value: {}'.format(k, v))
            target[k] = v


def fetch_defaults(binfos, patts=None, target=None):
    m = binfos['config'].get('defaults')
    if m:
        if target is None:
            target = binfos

        # get root defaults
        fill_default(target, m, patts)
        # platform dependent defaults
        plat = binfos['platform']
        fill_default(target, m.get(plat), patts)
        # platform > arch dependent defaults
        arch = binfos['arch']
        fill_default(target, m.get(plat, {}).get(arch), patts)


def fetch_available_features(binfos):
    compiler = binfos.get('compiler')
    m = binfos['config'].get('features', {})
    feats = m.get(compiler or '')
    if feats:
        feats = merge(feats, feats.get(binfos['arch']))
    binfos['features'] = feats or {}
    return feats


def join_flags(target, all_flags):
    if not all_flags:
        return

    for k, v in all_flags.iteritems():
        target[k] = ' '.join([target.get(k) or '', v])
        log.debug('Join "{}" with value: {}'.format(k, v))


def fill_features(binfos, enabled, target):
    if enabled:
        features = binfos.get('features')
        for x in enabled:
            if x == 'c++11':
                x = 'cxx11'
            feat = features.get(x)
            if feat is None:
                log.warn('Unsupported feature: %s', x)
            else:
                log.debug('Enabled feature "%s"', x)
                join_flags(target, feat)


def sub_rescure(d, **kargs):
    def sub(s):
        return string.Template(s).safe_substitute(**kargs)

    if isinstance(d, str):
        return sub(d)
    elif isinstance(d, dict):
        return dict((k, sub_rescure(v, **kargs)) for k, v in d.iteritems())
    else:
        return d


def prepare_sdk(binfos):
    ''' set "make" and "cmake" keys'''
    plat = binfos['platform']
    arch = binfos.get('arch')
    sdk = binfos.get('sdk')

    m = binfos['config'].get(plat)
    if m is None:
        return

    def get_var(vname):
        return get_variable(m, vname, arch, sdk)

    root = get_var('root')
    tcroot = os.path.join(os.path.dirname(__file__), 'toolchain')

    def get_var2(vname):
        d = get_var(vname)
        return sub_rescure(d, root=root, tcroot=tcroot) if d else None

    import parseutils as utils
    dm = dict(root=root,
              make=get_var2('make'),
              cmake=get_var2('cmake'),
              run=' && '.join(utils.get_list(get_var2('run')))
              )

    binfos['cross'] = dm

    return dict(system_libs=get_var2('system_libs'))


if __name__ == '__main__':
    cfg = load()
    import pprint
    pprint.PrettyPrinter().pprint(cfg)
