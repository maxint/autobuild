#ifndef __ARCSOFT_XXX_H__
#define __ARCSOFT_XXX_H__

#if defined(_WINDOWS) && defined(arcsoft_xxx_EXPORTS)
#   define ASXXX_DLL __declspec(dllexport)
#else
#   define ASXXX_DLL
#endif

#include "asvloffscreen.h"

#ifdef __cplusplus
extern "C" {
#endif
    
typedef struct
{
    MLong lCodebase;                ///< Codebase version number
    MLong lMajor;                   ///< major version number
    MLong lMinor;                   ///< minor version number
    MLong lBuild;                   ///< Build version number, increasable only
    MTChar* Version;                ///< version in string form
    MTChar* BuildDate;              ///< latest build Date
    MTChar* CopyRight;              ///< copyright
} ArcSoft_XXX_Version;  ///< Object Tracking Engine Version

//////////////////////////////////////////////////////////////////////////
/// @brief   The function used to get version information of the library.
///
/// @return const ArcSoft_XXX_Version *  [OUT]
///
/// @since Version 1.0
///
/// @details
///
/// @remark
///
/// @see
//////////////////////////////////////////////////////////////////////////
ASXXX_DLL const ArcSoft_XXX_Version * ArcSoft_XXX_GetVersion();

#ifdef __cplusplus
}
#endif

#endif // end of header file
