#/bin/env bash
if [ $# != 2 ]; then
	echo "usage: rmount <local shared dir> <target mount dir>"
else
    srcpath=$1
    if [[ $srcpath != //* ]]; then
       srcpath=//maxint-w7/$srcpath
    fi
	sudo mount -o username="arcsoft-hz/lny1856",uid=lny1856,nounix,noserverino "$srcpath" "$2"
fi
