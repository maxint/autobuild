# Autobuild Binary Dependencies

- Download [CMake](http://www.cmake.org/download/) and unpack them as following:
    * cmake-3.0.2-Darwin64-universal
    * cmake-3.0.2-win32-x86
    * cmake-3.1.0-rc1-Linux-i386

- Download [yaml.zip](https://bitbucket.org/maxint/autobuild/downloads/yaml.zip) (standalone python yaml package) or install pyyaml using pip
    
    $ pip install pyyaml
