#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Get platform code in version number.

\\gtao-compiler\Common Dependencies\mpbase\0.1.0.3\VersionNumber_Release.xls

'''

import logging

log = logging.getLogger('build')


def get_value(m, arch=None, sdk=None):
    var = m
    if isinstance(m, dict):
        # check > arch
        if arch:
            var = m.get(arch) or var

        mm = m.get('sdk')
        if mm and sdk:
            sdk_m = mm[sdk]
            if isinstance(sdk_m, dict):
                # check > sdk > arch
                var = sdk_m.get(arch) or var
            else:
                # check > sdk
                var = sdk_m or var

    return var


def get_compiler_code(m, plat, arch, sdk):
    mm = m['platform'].get(plat)
    assert mm, 'Unsupported platform "{}"'.format(plat)
    return get_value(mm, arch, sdk)


def get_arch_code(m, abi):
    return m['arch'].get(abi)


def get_platform_code(binfos):
    m = binfos['config']['platform_code']
    plat = binfos['platform']
    arch = binfos['arch']
    abi = binfos['abi']
    sdk = binfos.get('sdk')

    log.debug('platform=%s, abi=%s, sdk=%s', plat, abi, str(sdk))

    plat_code = get_compiler_code(m, plat, arch, sdk)
    arch_code = get_arch_code(m, abi)
    log.debug('Platform code: %s (%s)', str(plat_code), arch)
    log.debug('Architecture code: %s (%s)', str(arch_code), abi)
    assert isinstance(plat_code, int), 'Unknown platform code'
    assert isinstance(arch_code, int), 'Unknown architecture code'

    code = '{}{:02}'.format(plat_code, arch_code)
    log.debug('Full platform code: %s', code)
    binfos['platform_code'] = code

    return code
