#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright (C) 2014 maxint <NOT_SPAM_lnychina@gmail.com>
#
# Distributed under terms of the MIT license.

'''
Manage mpbase libraries.

May sub tasks:
- Get mpbase libraries from common server.
- Return include directory and libraries of multiple platform.
'''

import os
import re
import glob
import config

import logging
log = logging.getLogger('build')


def versions(m):
    vers = [x for x in m.keys() if re.match(r'\d+\.\d+\.\d+.\d+', x)]
    vers.sort(key=lambda s: map(int, s.split('.')))
    return vers


def latest_version(m):
    vers = versions(m)
    return vers[len(vers) - 1]


def default_version(m, ver=None):
    return ver or m.get('default') or latest_version(m)


def incs_libs2(binfos, m, version=None, shared=None):
    for arch in [binfos['abi'], binfos['arch']]:
        ret = incs_libs(m,
                        binfos['platform'],
                        arch,
                        binfos.get('sdk'),
                        version,
                        shared)
        if ret:
            return ret
    raise Exception('Can not find dependency for ' +
                    binfos['platform_abbr'] + ': ' +
                    m['name'] + ' ' + (version or ''))


def get_lib(pdir, shared):
    libdir = os.path.join(pdir, 'lib')
    libs = glob.glob(libdir + '/*')

    def find(ext):
        for x in libs:
            if x.endswith(ext):
                return x

    if libs:
        if shared is None:
            return find('.a') or find('.so') or find('.lib')
        elif shared:
            return find('.so') or find('.lib')
        else:
            # NOTE: static for default
            return find('.a') or find('.lib')


def incs_libs(m, platform, arch=None, sdk=None, version=None, shared=None):
    assert(platform)

    if version is None:
        version = default_version(m)

    assert 'root' in m, 'Please define root path'
    assert 'name' in m, 'Please define library name'

    root = m['root']
    name = m['name']

    vm = m.get(version)
    assert vm, 'Unsupported version "{}"'.format(version)

    lastpath = config.get_path(vm, platform, arch, sdk)
    finalpath = os.path.join(root, vm.get('path'), lastpath)
    assert finalpath

    if finalpath:
        tdir = finalpath
        if 'subdir' in m:
            tdir += os.sep + m.get('subdir')
        libs = get_lib(tdir, shared)
        if not libs:
            log.warn('Cannot find {} ({}) shared={} in "{}"'.format(name,
                                                                    version,
                                                                    shared,
                                                                    tdir))
            return None

        incdir = tdir + os.sep + 'inc'
        if 'incs' in m:
            incs = [incdir + os.sep + x for x in m.get('incs')]
        else:
            incs = glob.glob(incdir + '/*.h')
            incs += glob.glob(incdir + '/*.hpp')

        return dict(
            root=finalpath,
            pkgpath=m.get('pkgpath') or name,
            inc_dir=incdir,
            incs=incs,
            libs=libs,
        )

if __name__ == '__main__':
    cfg = config.load()['config']['depends']['mpbase']
    ver = '0.1.0.3'
    print incs_libs(cfg, 'windows', 'x86', 'vs2012', version=ver)
    print incs_libs(cfg, 'android', 'arm', version='0.1.0.4', shared=True)
    print incs_libs(cfg, 'android', 'arm', version=ver)
    print incs_libs(cfg, 'ios', 'armv7', version=ver)
    print incs_libs(cfg, 'ads', version=ver)
    print incs_libs(cfg, 'coach', version=ver)
    print incs_libs(cfg, 'unknown', 'unknown', version=ver)
