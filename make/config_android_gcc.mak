NDK_BASE:= /home/NDK/android-ndk-r9
PREF	:= $(NDK_BASE)/toolchains/arm-linux-androideabi-$(COMPILER_VERSION)/prebuilt/linux-x86/bin/arm-linux-androideabi-
CC =
CPP =

SYSROOT := $(NDK_BASE)/platforms/android-9/arch-arm
STLDIR 	:= $(NDK_BASE)/sources/cxx-stl/gabi++

# armv7
MACROS  = 
INCS	:= $(STLDIR)/include

CFLAGS  = -nostdlib 
CPPFLAGS = 
EXTRA_CFLAGS = --sysroot=$(SYSROOT)

LDFLAGS	= -Wl,-soname,$(TARGET_BASENAME) -Wl,--no-undefined \
		  -shared --sysroot=$(SYSROOT) \
		  -lc -lm -lstdc++ -lgcc -ldl -llog \
		  $(STLDIR)/libs/armeabi-v7a/libgabi++_static.a

include $(TOPDIR)make_gnu.mak
