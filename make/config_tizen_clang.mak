NDK_BASE:= /e/sbs/tizen-sdk
PREF	:= $(NDK_BASE)/tools/llvm-3.1/bin/llvm-
CC		:= $(NDK_BASE)/tools/llvm-3.1/bin/clang
CPP		:= $(NDK_BASE)/tools/llvm-3.1/bin/clang++

# armv7, tizen-arm
MACROS  = VER_PLAT_1=107
SYSROOT := $(NDK_BASE)/platforms/tizen2.2/rootstraps/tizen-device-2.2.native
CPPDIR  := $(SYSROOT)/usr/include/c++/4.5.3
GCC_TOOLCHAIN := $(NDK_BASE)/tools/arm-linux-gnueabi-gcc-4.5/

INCS	=  $(SYSROOT)/include $(CPPDIR) $(CPPDIR)/armv7l-tizen-linux-gnueabi

CFLAGS  = -march=armv7-a -fPIC -msoft-float -fno-short-enums \
		  -mfloat-abi=softfp -mfpu=neon -ffast-math
CPPFLAGS = -fno-rtti -fexceptions
EXTRA_CFLAGS = -target arm-tizen-linux-gnueabi \
			   -gcc-toolchain $(GCC_TOOLCHAIN) \
			   --sysroot=$(SYSROOT)

LDFLAGS	= -shared -target arm-tizen-linux-gnueabi \
		  -gcc-toolchain $(GCC_TOOLCHAIN) \
		  -Wl,-soname,$(TARGET_BASENAME) \
		  -Wl,--no-undefined \
		  --sysroot=$(SYSROOT) -lc -lm -lstdc++ -lgcc -ldl -lpthread -lrt

include $(TOPDIR)make_gnu.mak
