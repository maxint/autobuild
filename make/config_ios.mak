ifndef LOCAL_COMPILER
	COMPILER = clang
endif
ifndef LOCAL_COMPILER_VERSION
	ifeq ($(COMPILER),'gcc')
		COMPILER_VERSION = 4.4.3
	endif
	ifeq ($(LOCAL_COMPILER),'clang')
		COMPILER_VERSION = 5.0
	endif
endif

DYNAMIC_SUFFIX = .dylib

TOPDIR := $(dir $(lastword $(MAKEFILE_LIST)))
include $(TOPDIR)config_$(PLATFORM)_$(COMPILER).mak
