NDK_BASE= /Applications/Xcode.app/Contents/Developer
PREF   	= $(NDK_BASE)/Toolchains/XcodeDefault.xctoolchain/usr/bin/
CC      = $(PREF)clang
CPP     = $(PREF)clang++

# armv7, tizen-arm
MACROS  = VER_PLAT_1=164
SYSROOT := $(NDK_BASE)/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS7.0.sdk
CPPDIR  := $(PERF)/../lib/clang/5.0/include

INCS	=  $(SYSROOT)/include \
		   $(PERF)/../lib/clang/5.0/include \
		   $(PERF)/../lib/c++/v1

CFLAGS  = 
CPPFLAGS = 
EXTRA_CFLAGS = --sysroot=$(SYSROOT)

LDFLAGS	= -shared \
		  --sysroot=$(SYSROOT) -lc -lm -lstdc++ -ldl -lpthread

include $(TOPDIR)make_gnu.mak
