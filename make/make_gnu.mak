#----------------------------------------------------------
# Compiler commands

ifndef CC
	CC	:= $(PREF)gcc
endif
ifndef CPP
	CPP	:= $(PREF)g++
endif
AR		:= $(PREF)ar rc
AS		:= $(PREF)as
RAN		:= $(PREF)ranlib

FO		= 
CO		= -c -o

STATIC_PREFIX = 
STATIC_SUFFIX = .a
DYNAMIC_PREFIX = lib

#----------------------------------------------------------
# System commands

RM  = rm -f
RMDIR = rmdir -p --ignore-fail-on-non-empty
MKDIR = mkdir -p

#----------------------------------------------------------
# Prepare

OUTDIR = $(OBJDIR)/output
VPATH = .

#$(warning now we reached ifeq BUILD_TYPE=$(BUILD_TYPE))
ifeq ($(BUILD_TYPE),static)
	TARGET_BASENAME := $(STATIC_PREFIX)$(LOCAL_NAME)$(STATIC_SUFFIX)
endif
ifeq ($(BUILD_TYPE),shared)
	TARGET_BASENAME := $(DYNAMIC_PREFIX)$(LOCAL_NAME)$(DYNAMIC_SUFFIX)
endif
TARGET_OUT 	:= $(OUTDIR)/$(TARGET_BASENAME)

OBJS		:= $(LOCAL_SRCS:%.cpp=%.o)
OBJS		:= $(OBJS:%.c=%.o)
OBJS		:= $(addprefix $(OBJDIR)/, $(OBJS))

CFLAGS 		:= $(CFLAGS) -O2 $(LOCAL_CFLAGS)
CPPFLAGS	:= $(CPPFLAGS) $(LOCAL_CPPFLAGS)
CFLAGS0		:= $(CFLAGS) $(EXTRA_CFLAGS) $(LOCAL_EXTRA_CFLAGS) -Wall -Wno-unused-function -Wno-parentheses $(MACROS:%=-D%) $(INCS:%=-I%)
CFLAGS1		:= $(LOCAL_DEFS:%=-D%) $(LOCAL_INCS:%=-I%)
CCOMPILE    := $(CC) $(CFLAGS0) $(CFLAGS1) $(CO)
CPPCOMPILE  := $(CPP) $(CFLAGS0) $(CFLAGS1) $(CPPFLAGS) $(EXTRA_CPPFLAGS) $(LOCAL_EXTRA_CPPFLAGS) $(CO)

.SECONDARY: $(OBJS)

$(OBJDIR)/%.o: %.c
	@echo == Compiling $<
	@$(MKDIR) $(dir $@)
	@$(CCOMPILE) $@ $< 

$(OBJDIR)/%.o: %.cpp
	@echo == Compiling $<
	@$(MKDIR) $(dir $@)
	@$(CPPCOMPILE) $@ $< 
	
#----------------------------------------------------------
# Make Rules

.PHONY: all lib prepare clean dist version dump

DUMP_FILE := $(OBJDIR).yaml

all: lib dump
lib: prepare $(TARGET_OUT)
dump: $(DUMP_FILE)

VER_OBJ := $(OBJDIR)/$(VERSION_FILE:%.cpp=%.o)
VER_OBJ := $(VER_OBJ:%.c=%.o)
#.PHONY: $(VER_OBJ)

prepare:
	@echo == Clean output directory.
	@-$(MKDIR) $(OUTDIR)
	@-$(RM) -rf $(OUTDIR)/*
	@-$(RM) -f $(VER_OBJ)
	@echo -- Deleting $(VER_OBJ)
	@echo -- Compile option BEGIN --
	@echo $(CPPCOMPILE)
	@echo -- Compile option END --

# aux make rules
%$(STATIC_SUFFIX): $(OBJS)
	@echo == Creating static library "$@"...
	@$(AR) $@ $^
	@$(RAN) $@

%$(DYNAMIC_SUFFIX): $(OBJS)
	@echo == Creating dynamic library "$@"...
	@echo -- Link option BEGIN --
	@echo $(CC) $(LDFLAGS) $(LOCAL_LDFLAGS)
	@echo -- Link option END --
	@$(CC) -o $@ $^ $(LDFLAGS) $(LOCAL_LDFLAGS)

clean:
	@echo == Clean generated files and dirs.
	@-$(RM) -rf $(OBJDIR)
	@-$(RMDIR) obj

version: $(TARGET_OUT)
	@echo Version of "$^"
	@grep -oa "ArcSoft_[a-zA-Z]*_[0-9.]\{1,15\}[^)]*)" $^

$(DUMP_FILE): $(OBJDIR).mak
	@echo == Dumping compiling info to "$@"...
	@echo "cflags: $(CFLAGS) $(CPPFLAGS)" > $@
	@echo "location: $(TARGET_OUT)" >> $@

TEST_OUT := $(OBJDIR)/test$(EXE_SUFFIX)
$(TEST_OUT): testbed/cmd/main.cpp $(TARGET_OUT)
	@echo "== Creating test application"
	@$(CC) $(CFLAGS_EXTRA) -g $^ $(LOCAL_TEST_FLAGS) $(TEST_FLAGS) -lstdc++ -o $(TEST_OUT)

test: $(TESTOUT)

# -- man --
# basename 	$(notdir <var>)
# dirname  	$(dir <var>)
# full path	$(realpath <var>)
